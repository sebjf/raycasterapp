#Ray Caster#

In computer graphics, ray casting is the process of generating an image by casting a ray into a virtual environment and determining the luminance at the intersection point of that ray. The luminance defines the colour and intensity of the light transported along the ray. Therefore, if this is done for a ray corresponding to each pixel in a frame, an image is produced.


Introduction
------------

This application demonstrates real-time ray casting on a Dataflow Engine. The DFE ray-caster casts one ray for each pixel on a physical display. It performs the intersection tests with geometry stored in hardware. The result of the intersection test is a location on a plane. The colour of this location is read from a texture map applied to the plane. This colour determines the luminance transported along that ray from the environment to the virtual camera.

Ray Casting differs from Ray Tracing, only in the number of reflections. In Ray Tracing, each ray after intersecting the environment will cast one or more rays from that location into the scene, and this process will repeat recursively for an arbitrary amount of time. Using lighting simulation calculations, the amount of light, or luminance, transported along each ray, from the intersection point, to the point from which it was cast, can be computed. Summing the luminance computations will give the luminance, or colour, of the initial cast. In Ray Casting, no secondary rays are cast. Ray Casting typically will cast many rays for a single pixel (in all directions) in order to get a statistically meaningful estimation of the luminance reaching that point in space. We limit the number of casts and instead precompute highly detailed environment maps, in order to make our implementation as low latency as possible.

Using a distortion map, we can map pixels on the real display, to arbitrary locations on a virtual viewplane, which can be any shape or size. This allows us to render all sorts of distortions, including barrel distortion compensation for modern HMDs, during rendering avoiding the expensive post-rendering warping stage required on common GPUs.

Using tiling and mip mapping to reduce expensive and non-deterministic LMEM reads, this design can run at pixel rate in hardware (165 MHz) to drive the Oculus Rift DK2. Using special hardware the DFE can drive a display directly. Otherwise, it can drive a virtual monitor on the host system.


Usage
-----

1. Clone the repository to your local machine. You will need SDL and SDL_image in order to build the CPU code. Download and install these with `yum`
2. Build by executing `make RUNRULE="Simulation"` in `CPUCode/`
3. Start the simulator by navigating to `RunRules/Simulation/` and executing `make startsim`
4. Now the application can be executed by giving the command `./binaries/RayCaster`


Features
--------

- Real-Time Ray Casting on a DFE
- Performs intersection tests in hardware
- Uses tiling to cache and reuse LMEM reads
- Uses mip-mapping to maximise LMEM cache reuse
- Uses low latency PCIe streams to send tracking data to DFE in under 1 ms (tested on real hardware)
- Includes virtual monitor to demonstrate rendering in simulation